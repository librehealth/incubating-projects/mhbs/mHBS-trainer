var pageUsages = {};

routes = [
  {
    path: '/',
    id: 'home',
    url: './pages/homepage.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        // increase page visits
        increasePageVisits("Wvcu1bXFIvD","homepage");

        // start obseriving time spent by user on this page
        pageUsages['Wvcu1bXFIvD'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.Wvcu1bXFIvD!=null && pageUsages.Wvcu1bXFIvD.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.Wvcu1bXFIvD.start_time;
          // reset start time to 0
          pageUsages.Wvcu1bXFIvD.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("Wvcu1bXFIvD",time_spent,"homepage");
        }
      }
    }
  },
  {
    path: '/about/',
    url: './pages/about.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("IXx6hyabfgr","about");
        // start obseriving time spent by user on this page
        pageUsages['IXx6hyabfgr'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.IXx6hyabfgr!=null && pageUsages.IXx6hyabfgr.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.IXx6hyabfgr.start_time;
          // reset start time to 0
          pageUsages.IXx6hyabfgr.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("IXx6hyabfgr",time_spent,"about");
        }
      }
    }
  },
  //<editor-fold desc="mHBS Guide Routes" defaultstate="collapsed">
  {
    path: '/mediaPage/',
    url: './pages/mediaPage.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("lewNGzUFDnS","mediaPage");
        // start obseriving time spent by user on this page
        pageUsages['lewNGzUFDnS'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.lewNGzUFDnS!=null && pageUsages.lewNGzUFDnS.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.lewNGzUFDnS.start_time;
          // reset start time to 0
          pageUsages.lewNGzUFDnS.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("lewNGzUFDnS",time_spent,"mediaPage");
        }
      }
    }
  },
  {
    path: '/page1/',
    url: './pages/page1.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("bA8QypQ8EOy","section1");
        // start obseriving time spent by user on this page
        pageUsages['bA8QypQ8EOy'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.bA8QypQ8EOy!=null && pageUsages.bA8QypQ8EOy.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.bA8QypQ8EOy.start_time;
          // reset start time to 0
          pageUsages.bA8QypQ8EOy.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("bA8QypQ8EOy",time_spent,"section1");
        }
      }
    }
  },
  {
    path: '/section1/',
    url: './pages/section1.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("aZ8iXoPhI8J","section1");
        // start obseriving time spent by user on this page
        pageUsages['aZ8iXoPhI8J'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.aZ8iXoPhI8J!=null && pageUsages.aZ8iXoPhI8J.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.aZ8iXoPhI8J.start_time;
          // reset start time to 0
          pageUsages.aZ8iXoPhI8J.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("aZ8iXoPhI8J",time_spent,"section1");
        }
      }
    }
  },
  {
    path: '/section1a/',
    url: './pages/section1a.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("OGyg1ZBPTyT");
        // start obseriving time spent by user on this page
        pageUsages['OGyg1ZBPTyT'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.OGyg1ZBPTyT!=null && pageUsages.OGyg1ZBPTyT.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.OGyg1ZBPTyT.start_time;
          // reset start time to 0
          pageUsages.OGyg1ZBPTyT.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("OGyg1ZBPTyT",time_spent,"section1a");
        }
      }
    }
  },
  {
    path: '/section1b/',
    url: './pages/section1b.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("bh2d52QhJDU","section1b");
        // start obseriving time spent by user on this page
        pageUsages['bh2d52QhJDU'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.bh2d52QhJDU!=null && pageUsages.bh2d52QhJDU.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.bh2d52QhJDU.start_time;
          // reset start time to 0
          pageUsages.bh2d52QhJDU.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("bh2d52QhJDU",time_spent,"section1b");
        }
      }
    }
  },
  {
    path: '/section1c/',
    url: './pages/section1c.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("pL8YMHAavzM","section1c");
        // start obseriving time spent by user on this page
        pageUsages['pL8YMHAavzM'] = {"start_time" : new Date(),pL8YMHAavzM : false};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.pL8YMHAavzM!=null && pageUsages.pL8YMHAavzM.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.pL8YMHAavzM.start_time;
          // reset start time to 0
          pageUsages.pL8YMHAavzM.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("pL8YMHAavzM",time_spent,"section1c");
        }
      }
    }
  },
  {
    path: '/section2/',
    url: './pages/section2.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("rCBuWSqHRUo","section2");
        // start obseriving time spent by user on this page
        pageUsages['rCBuWSqHRUo'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.rCBuWSqHRUo!=null && pageUsages.rCBuWSqHRUo.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.rCBuWSqHRUo.start_time;
          // reset start time to 0
          pageUsages.rCBuWSqHRUo.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("rCBuWSqHRUo",time_spent,section2);
        }
      }
    }
  },
  {
    path: '/section2a/',
    url: './pages/section2a.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("S3a0I7YoKZZ","section2a");
        // start obseriving time spent by user on this page
        pageUsages['S3a0I7YoKZZ'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.S3a0I7YoKZZ!=null && pageUsages.S3a0I7YoKZZ.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.S3a0I7YoKZZ.start_time;
          // reset start time to 0
          pageUsages.S3a0I7YoKZZ.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("S3a0I7YoKZZ",time_spent,"section2a");
        }
      }
    }
  },
  {
    path: '/section2b/',
    url: './pages/section2b.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("eU8z6djmCNo","section2b");
        // start obseriving time spent by user on this page
        pageUsages['eU8z6djmCNo'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.eU8z6djmCNo!=null && pageUsages.eU8z6djmCNo.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.eU8z6djmCNo.start_time;
          // reset start time to 0
          pageUsages.eU8z6djmCNo.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("eU8z6djmCNo",time_spent,"section2b");
        }
      }
    }
  },
  {
    path: '/section2c/',
    url: './pages/section2c.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("aJ4sxqJE39N","section2c");
        // start obseriving time spent by user on this page
        pageUsages['aJ4sxqJE39N'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.aJ4sxqJE39N!=null && pageUsages.aJ4sxqJE39N.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.aJ4sxqJE39N.start_time;
          // reset start time to 0
          pageUsages.aJ4sxqJE39N.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("aJ4sxqJE39N",time_spent,"section2c");
        }
      }
    }
  },
  {
    path: '/section2d/',
    url: './pages/section2d.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("JAMaU6orBTC","section2d");
        // start obseriving time spent by user on this page
        pageUsages['JAMaU6orBTC'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.JAMaU6orBTC!=null && pageUsages.JAMaU6orBTC.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.JAMaU6orBTC.start_time;
          // reset start time to 0
          pageUsages.JAMaU6orBTC.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("JAMaU6orBTC",time_spent,"section2d");
        }
      }
    }
  },
  {
    path: '/section2e/',
    url: './pages/section2e.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("fZFOhojmLck","section2e");
        // start obseriving time spent by user on this page
        pageUsages['fZFOhojmLck'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.fZFOhojmLck!=null && pageUsages.fZFOhojmLck.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.fZFOhojmLck.start_time;
          // reset start time to 0
          pageUsages.fZFOhojmLck.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("fZFOhojmLck",time_spent,"section2e");
        }
      }
    }
  },
  {
    path: '/section3/',
    url: './pages/section3.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("DmiccJV9owi","section3");
        // start obseriving time spent by user on this page
        pageUsages['DmiccJV9owi'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.DmiccJV9owi!=null && pageUsages.DmiccJV9owi.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.DmiccJV9owi.start_time;
          // reset start time to 0
          pageUsages.DmiccJV9owi.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("DmiccJV9owi",time_spent,"section3");
        }
      }
    }
  },
  {
    path: '/section3a/',
    url: './pages/section3a.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("i1uNZG57Vvn","section3a");
        // start obseriving time spent by user on this page
        pageUsages['i1uNZG57Vvn'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.i1uNZG57Vvn!=null && pageUsages.i1uNZG57Vvn.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.i1uNZG57Vvn.start_time;
          // reset start time to 0
          pageUsages.i1uNZG57Vvn.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("i1uNZG57Vvn",time_spent,"section3a");
        }
      }
    }
  },
  {
    path: '/section3b/',
    url: './pages/section3b.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("GHdoj7apebR","section3b");
        // start obseriving time spent by user on this page
        pageUsages['GHdoj7apebR'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.GHdoj7apebR!=null && pageUsages.GHdoj7apebR.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.GHdoj7apebR.start_time;
          // reset start time to 0
          pageUsages.GHdoj7apebR.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("GHdoj7apebR",time_spent,"section3b");
        }
      }
    }
  },
  {
    path: '/section3c/',
    url: './pages/section3c.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("s5ExNdjXBat","section3c");
        // start obseriving time spent by user on this page
        pageUsages['s5ExNdjXBat'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.s5ExNdjXBat!=null && pageUsages.s5ExNdjXBat.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.s5ExNdjXBat.start_time;
          // reset start time to 0
          pageUsages.s5ExNdjXBat.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("s5ExNdjXBat",time_spent,"section3c");
        }
      }
    }
  },
  {
    path: '/section3d/',
    url: './pages/section3d.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("yTHtEpk35ic"),"section3d";
        // start obseriving time spent by user on this page
        pageUsages['yTHtEpk35ic'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.yTHtEpk35ic!=null && pageUsages.yTHtEpk35ic.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.yTHtEpk35ic.start_time;
          // reset start time to 0
          pageUsages.yTHtEpk35ic.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("yTHtEpk35ic",time_spent,"section3d");
        }
      }
    }
  },
  {
    path: '/section3e/',
    url: './pages/section3e.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("YJjv8U9rLgB","section3e");
        // start obseriving time spent by user on this page
        pageUsages['YJjv8U9rLgB'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.YJjv8U9rLgB!=null && pageUsages.YJjv8U9rLgB.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.YJjv8U9rLgB.start_time;
          // reset start time to 0
          pageUsages.YJjv8U9rLgB.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("YJjv8U9rLgB",time_spent,"section3e");
        }
      }
    }
  },
  {
    path: '/section3f/',
    url: './pages/section3f.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("Zpa8oIgQskR","section3f");
        // start obseriving time spent by user on this page
        pageUsages['Zpa8oIgQskR'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.Zpa8oIgQskR!=null && pageUsages.Zpa8oIgQskR.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.Zpa8oIgQskR.start_time;
          // reset start time to 0
          pageUsages.Zpa8oIgQskR.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("Zpa8oIgQskR",time_spent,"section3f");
        }
      }
    }
  },
  {
    path: '/section3g/',
    url: './pages/section3g.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("nHVwOmfFbK5","section3g");
        // start obseriving time spent by user on this page
        pageUsages['nHVwOmfFbK5'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.nHVwOmfFbK5!=null && pageUsages.nHVwOmfFbK5.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.nHVwOmfFbK5.start_time;
          // reset start time to 0
          pageUsages.nHVwOmfFbK5.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("nHVwOmfFbK5",time_spent,"section3g");
        }
      }
    }
  },
  {
    path: '/section4/',
    url: './pages/section4.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("nOCGR7oWXhZ","section4");
        // start obseriving time spent by user on this page
        pageUsages['nOCGR7oWXhZ'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.nOCGR7oWXhZ!=null && pageUsages.nOCGR7oWXhZ.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.nOCGR7oWXhZ.start_time;
          // reset start time to 0
          pageUsages.nOCGR7oWXhZ.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("nOCGR7oWXhZ",time_spent,"section4");
        }
      }
    }
  },
  {
    path: '/section4a/',
    url: './pages/section4a.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("lronaO33ppS","section4a");
        // start obseriving time spent by user on this page
        pageUsages['lronaO33ppS'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.lronaO33ppS!=null && pageUsages.lronaO33ppS.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.lronaO33ppS.start_time;
          // reset start time to 0
          pageUsages.lronaO33ppS.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("lronaO33ppS",time_spent,"section4a");
        }
      }
    }
  },
  {
    path: '/section4b/',
    url: './pages/section4b.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("D6JG7GBPuIH","section4b");
        // start obseriving time spent by user on this page
        pageUsages['D6JG7GBPuIH'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.D6JG7GBPuIH!=null && pageUsages.D6JG7GBPuIH.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.D6JG7GBPuIH.start_time;
          // reset start time to 0
          pageUsages.D6JG7GBPuIH.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("D6JG7GBPuIH",time_spent,"section4b");
        }
      }
    }
  },
  {
    path: '/section4c/',
    url: './pages/section4c.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("tUuhRqEXJSf","section4c");
        // start obseriving time spent by user on this page
        pageUsages['tUuhRqEXJSf'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.tUuhRqEXJSf!=null && pageUsages.tUuhRqEXJSf.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.tUuhRqEXJSf.start_time;
          // reset start time to 0
          pageUsages.tUuhRqEXJSf.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("tUuhRqEXJSf",time_spent,"section4c");
        }
      }
    }
  },
  {
    path: '/section4d/',
    url: './pages/section4d.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("a1X0TLZ3uk9","section4d");
        // start obseriving time spent by user on this page
        pageUsages['a1X0TLZ3uk9'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.a1X0TLZ3uk9!=null && pageUsages.a1X0TLZ3uk9.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.a1X0TLZ3uk9.start_time;
          // reset start time to 0
          pageUsages.a1X0TLZ3uk9.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("a1X0TLZ3uk9",time_spent,"section4d");
        }
      }
    }
  },
  {
    path: '/section4e/',
    url: './pages/section4e.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("syoRjkPpuLX","section4e");
        // start obseriving time spent by user on this page
        pageUsages['syoRjkPpuLX'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.syoRjkPpuLX!=null && pageUsages.syoRjkPpuLX.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.syoRjkPpuLX.start_time;
          // reset start time to 0
          pageUsages.syoRjkPpuLX.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("syoRjkPpuLX",time_spent,"section4e");
        }
      }
    }
  },
  {
    path: '/section5/',
    url: './pages/section5.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("rOSSrVAg3wm","section5");
        // start obseriving time spent by user on this page
        pageUsages['rOSSrVAg3wm'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.rOSSrVAg3wm!=null && pageUsages.rOSSrVAg3wm.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.rOSSrVAg3wm.start_time;
          // reset start time to 0
          pageUsages.rOSSrVAg3wm.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("rOSSrVAg3wm",time_spent,"section5");
        }
      }
    }
  },
  {
    path: '/section5a/',
    url: './pages/section5a.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("IoJXWMVxkM2","section5a");
        // start obseriving time spent by user on this page
        pageUsages['IoJXWMVxkM2'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.IoJXWMVxkM2!=null && pageUsages.IoJXWMVxkM2.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.IoJXWMVxkM2.start_time;
          // reset start time to 0
          pageUsages.IoJXWMVxkM2.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("IoJXWMVxkM2",time_spent,"section5a");
        }
      }
    }
  },
  {
    path: '/section5b',
    url: './pages/section5b.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("i9a3kUlUNuY","section5b");
        // start obseriving time spent by user on this page
        pageUsages['i9a3kUlUNuY'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.i9a3kUlUNuY!=null && pageUsages.i9a3kUlUNuY.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.i9a3kUlUNuY.start_time;
          // reset start time to 0
          pageUsages.i9a3kUlUNuY.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("i9a3kUlUNuY",time_spen,"section5bt");
        }
      }
    }
  },
  {
    path: '/section6/',
    url: './pages/section6.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("iOvJqoJthkR","section6");
        // start obseriving time spent by user on this page
        pageUsages['iOvJqoJthkR'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.iOvJqoJthkR!=null && pageUsages.iOvJqoJthkR.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.iOvJqoJthkR.start_time;
          // reset start time to 0
          pageUsages.iOvJqoJthkR.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("iOvJqoJthkR",time_spent,"section6");
        }
      }
    }
  },
  {
    path: '/section6a/',
    url: './pages/section6a.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("m9JUI66nrT8","section6a");
        // start obseriving time spent by user on this page
        pageUsages['m9JUI66nrT8'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.m9JUI66nrT8!=null && pageUsages.m9JUI66nrT8.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.m9JUI66nrT8.start_time;
          // reset start time to 0
          pageUsages.m9JUI66nrT8.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("m9JUI66nrT8",time_spent,"section6a");
        }
      }
    }
  },
  {
    path: '/section6b/',
    url: './pages/section6b.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("ZkQkWThW42a","section6b");
        // start obseriving time spent by user on this page
        pageUsages['ZkQkWThW42a'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.ZkQkWThW42a!=null && pageUsages.ZkQkWThW42a.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.ZkQkWThW42a.start_time;
          // reset start time to 0
          pageUsages.ZkQkWThW42a.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("ZkQkWThW42a",time_spent,"section6b");
        }
      }
    }
  },
  {
    path: '/section6c/',
    url: './pages/section6c.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("Jakw1ocIKB9","section6c");
        // start obseriving time spent by user on this page
        pageUsages['Jakw1ocIKB9'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.Jakw1ocIKB9!=null && pageUsages.Jakw1ocIKB9.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.Jakw1ocIKB9.start_time;
          // reset start time to 0
          pageUsages.Jakw1ocIKB9.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("Jakw1ocIKB9",time_spent, "section6c");
        }
      }
    }
  },
  {
    path: '/section6d/',
    url: './pages/section6d.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("ibjg6GDeuyF","section6d");
        // start obseriving time spent by user on this page
        pageUsages['ibjg6GDeuyF'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.ibjg6GDeuyF!=null && pageUsages.ibjg6GDeuyF.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.ibjg6GDeuyF.start_time;
          // reset start time to 0
          pageUsages.ibjg6GDeuyF.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("ibjg6GDeuyF",time_spent,"section6d");
        }
      }
    }
  },
  {
    path: '/section6e/',
    url: './pages/section6e.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("Qiz257H2PPx","section6e");
        // start obseriving time spent by user on this page
        pageUsages['Qiz257H2PPx'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.Qiz257H2PPx!=null && pageUsages.Qiz257H2PPx.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.Qiz257H2PPx.start_time;
          // reset start time to 0
          pageUsages.Qiz257H2PPx.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("Qiz257H2PPx",time_spent,"section6e");
        }
      }
    }
  },
  {
    path: '/section7/',
    url: './pages/section7.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("EF9kQZgoXmb","section7");
        // start obseriving time spent by user on this page
        pageUsages['EF9kQZgoXmb'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.EF9kQZgoXmb!=null && pageUsages.EF9kQZgoXmb.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.EF9kQZgoXmb.start_time;
          // reset start time to 0
          pageUsages.EF9kQZgoXmb.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("EF9kQZgoXmb",time_spent,"section7");
        }
      }
    }
  },
  //</editor-fold>
  {
    path: '/mhbsmain/',
    id: 'mhbsmain',
    url: './pages/mhbsmain.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        // increase page visits
        increasePageVisits("Qp64sTi4jam","mhbsmain");
        // start obseriving time spent by user on this page
        pageUsages['Qp64sTi4jam'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.Qp64sTi4jam!=null && pageUsages.Qp64sTi4jam.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.Qp64sTi4jam.start_time;
          // reset start time to 0
          pageUsages.Qp64sTi4jam.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("Qp64sTi4jam",time_spent,"mhbsmain");
        }
      }
    }
  },
  {
    path: '/videoList/',
    id: 'videoList',
    pushState: true,
    on: {
      pageBeforeIn: function (event, page) {
        /*
        console.log(event);
        // Router instance
        var router = this;
        // App instance
        var app = router.app;
        console.log(event + page);
        app.triggerOnlineContent();
        */
      }
    },
    async: function (routeTo, routeFrom, resolve, reject) {
      // Router instance
      var router = this;

      // App instance
      var app = router.app;

      if (app.data.videoList.length > 0) {
        resolve({
          componentUrl: './pages/videoList.html'
        });
      }
      else {
        if (app.data.offlineMode) {
          alert("Please activate wifi to download content");
        } else {
          app.methods.triggerOnlineContent();
        }
        reject({
          url: routeFrom
        })
      }
    }
  },
  // privacy policy route
  {
    path: '/privacypolicy',
    url: './pages/privacypolicy.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("euB2LAVkMNn","privacypolicy");
        // start obseriving time spent by user on this page
        pageUsages['euB2LAVkMNn'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.euB2LAVkMNn!=null && pageUsages.euB2LAVkMNn.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.euB2LAVkMNn.start_time;
          // reset start time to 0
          pageUsages.euB2LAVkMNn.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("euB2LAVkMNn",time_spent,"privacypolicy");
        }
      }
    }
  },
  // Default route (404 page). MUST BE THE LAST
  {
    path: '(.*)',
    url: './pages/404.html',
    pushState: true,
    on: {
      pageAfterIn: function (event, page) {
        increasePageVisits("qUVwmmUzZCZ","404");
        // start obseriving time spent by user on this page
        pageUsages['qUVwmmUzZCZ'] = {"start_time" : new Date()};
      },
      pageBeforeOut: function (event, page) {
        if(pageUsages.qUVwmmUzZCZ!=null && pageUsages.qUVwmmUzZCZ.start_time!=null){
          // calculate time spent by user on this page
          var time_spent = (new Date()) - pageUsages.qUVwmmUzZCZ.start_time;
          // reset start time to 0
          pageUsages.qUVwmmUzZCZ.start_time = 0;
          // add time spent to ocal db
          increasePageTimeSpent("qUVwmmUzZCZ",time_spent,"404");
        }
      }
    }
  },
];

// update page visits in db
function increasePageVisits(id,page_name){
  // Router instance
  var router = this;
  // App instance
  var app = router.app;
  //TODO: disabling WIP. Improve approach
  //app.methods.increasePageVisits(id,page_name);
}

// update page time spent in db
function increasePageTimeSpent(id,time_spent,page_name){
  // Router instance
  var router = this;
  // App instance
  var app = router.app;
  //TODO: disabling WIP. Improve approach
  //app.methods.increasePageTimeSpent(id,time_spent,page_name);
}
